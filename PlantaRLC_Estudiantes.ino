/* 
  Code by: Álvaro Javier Vargas Miranda ajvargasm@unal.edu.co
*/
#include <SPI.h>

#define sl Serial

/* 
 *  El modulo SPI del microcontrolador esta internamente conectado
 *  a los pines que se indican en la tabla, por lo tanto no los debe
 *  declarar en la configuración, unicamente se declara el pin CS.
| Arduino	| DAC	|
|-----------|-------|
| 13		| SCLK 	|
| 12		| DOUT 	|
| 11		| DIN 	|
| 08		| CS 	|
*/
// Pines
const int csDAC = 8;
const int nodeA = A0;
const int nodeB = A1;
const int DAC_CAL = A2;

// Variables
float vMAX = 0.0;    // Voltaje máximo de salida del DAC
float voltOut = 4.0; // Voltaje de salida del DAC

unsigned long sampleStamp = 0;

/* Funciones */
void actuactorOutput(double volt);

void setup()
{
  //Setup Serial
  sl.begin(115200); //Conserve este valor para poder tener tiempos bajos de muestreo
  //Setup SPI
  SPI.begin();
  //Setup PINES
  pinMode(csDAC, OUTPUT);
  digitalWrite(csDAC, 0x01);
  //Setup DAC
  float vRead = 0;
  /*Este procedimiento es obligatorio para calibrar la salida del DAC*/
  for (byte i = 1; i <= 4; i++)
  {
    vRead += analogRead(DAC_CAL) * (5.0 / 1023.0) * 2.0;
    delay(250);
  }
  vMAX = vRead / 4.0;
  //Start
  sl.print("\n#Program Start!\n");
  sl.println("Vc, Il, uC");
  actuactorOutput(0.0);
  delay(1000);
}

void loop()
{
  voltOut = 0.0;
  // Marca de tiempo de inicio
  sampleStamp = micros();
  // Voltaje de actuador
  actuactorOutput(voltOut);
  // Lectura de voltajes
  double vA = analogRead(nodeA) * (5.0 / 1023.0);
  double vB = analogRead(nodeB) * (5.0 / 1023.0);

  double vC = vA;
  double iL = (vB - vA) / 220;
  // Impresion de datos
  sl.print(vC, 4);
  sl.print("; ");
  sl.print(iL, 5);
  sl.print("; ");
  sl.print(voltOut, 4);
  sl.println(" ");

  //Marca de tiempo final
  sampleStamp = micros() - sampleStamp;
  delayMicroseconds(7e03 - sampleStamp); // Garantiza el tiempo de muestreo
}

void actuactorOutput(double volt)
{
  unsigned int bitValue;
  bitValue = (volt * (1023.0 / vMAX));
  bitValue = (bitValue & 0x3ff) << 2;

  // Selecciona el DAC
  digitalWrite(csDAC, 0x00);

  // Activa la transmision
  SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
  SPI.transfer16(bitValue);
  SPI.endTransaction();

  // Libera el DAC
  digitalWrite(csDAC, 0x01);
}
